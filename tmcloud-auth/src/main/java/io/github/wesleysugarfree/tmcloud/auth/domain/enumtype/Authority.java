package io.github.wesleysugarfree.tmcloud.auth.domain.enumtype;

public enum Authority {
    ROLE_USER, ROLE_ADMIN
}
